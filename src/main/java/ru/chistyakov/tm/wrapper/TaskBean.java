package ru.chistyakov.tm.wrapper;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.chistyakov.tm.api.IProjectService;
import ru.chistyakov.tm.api.ITaskService;
import ru.chistyakov.tm.entity.Task;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import java.util.List;

@ManagedBean
@SessionScoped
@Named("taskBean")
public class TaskBean {

    private IProjectService projectService;
    private ITaskService taskService;

    private List<Task> taskList;

    @Autowired
    public void setProjectService(@NotNull final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Autowired
    public void setTaskService(ITaskService taskService) {
        this.taskService = taskService;
    }


    public List<Task> getTaskList(@NotNull final String projectId) {
        taskList = taskService.findByProjectId(projectId);
        return taskList;
    }

    public String findTaskList(String projectId) {
        final List<Task> taskList = taskService.findByProjectId(projectId);
        return "/taskListJSF.xhtml";
    }
}
