package ru.chistyakov.tm.wrapper;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.chistyakov.tm.api.IProjectService;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.enumerate.ReadinessStatus;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import java.util.List;

@ManagedBean
@SessionScoped
@Named("projectBean")
public class ProjectBean {

    private IProjectService projectService;

    private List<Project> projectList;

    private String projectId;

    private Project project;

    @Autowired
    public void setProjectService(@NotNull final IProjectService projectService) {
        this.projectService = projectService;
    }

    public List<Project> getProjectList() {
        projectList = projectService.findAll();
        return projectList;
    }

    public Project getProject() {
        return project;
    }

    public String getProjectId() {
        return projectId;
    }

    public Project getOneProject(final @Nullable String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return projectService.findOne(projectId);
    }

    public String findTaskList(@Nullable final String projectId) {
        this.projectId = projectId;
        return "taskListJSF.xhtml";
    }

    public List<Project> deleteProject(@Nullable final String projectId) {
        projectService.remove(projectId);
        projectList = projectService.findAll();
        return projectList;
    }

    @NotNull
    public String editProject(@Nullable final String projectId) {
        this.projectId = projectId;
        return "projectEditJSF.xhtml";
    }

    @NotNull
    public String newProject() {
        project = new Project();
        return "newProjectJSF.xhtml";
    }

    @NotNull
    public String createProject() {
        project.setReadinessStatus(ReadinessStatus.PLANNED);
        projectService.persist(project);
        projectList = projectService.findAll();
        return "projectListJSF.xhtml";
    }
}
