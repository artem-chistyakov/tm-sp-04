package ru.chistyakov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.chistyakov.tm.entity.Project;

@Repository
public interface IProjectRepository extends JpaRepository<Project, String> {

}
