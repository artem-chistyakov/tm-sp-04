package ru.chistyakov.tm.dto;


import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.enumerate.ReadinessStatus;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;


@EqualsAndHashCode(callSuper = true)
@Data
@XmlRootElement(name = "project")
@Entity
@Table(name = "projects")
public final class ProjectDTO extends AbstractDTO implements Serializable {

    @NotNull
    @Column(name = "name",nullable = false)
    private String name = "";

    @NotNull
    @Column(name = "readiness_status",nullable = false)
    @Enumerated(EnumType.STRING)
    private ReadinessStatus readinessStatus = ReadinessStatus.PLANNED;

    @Nullable
    private String description;

    @Nullable
    @Column(name = "date_begin")
    private Date dateBeginProject;
    @Nullable
    @Column(name = "date_end")
    private Date dateEndProject;
}

