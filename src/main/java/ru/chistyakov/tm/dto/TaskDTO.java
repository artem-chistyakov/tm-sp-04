package ru.chistyakov.tm.dto;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.enumerate.ReadinessStatus;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@XmlRootElement(name = "task")
@Entity
@Table(name = "tasks")
public final class TaskDTO extends AbstractDTO implements Serializable {

    @Nullable
    @Column(name = "project_id", nullable = false, updatable = false)
    private String projectId;
    @NotNull
    @Column(name = "name", nullable = false)
    private String name = "";
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "readiness_status", nullable = false)
    private ReadinessStatus readinessStatus = ReadinessStatus.PLANNED;
    @Nullable
    private String description;
    @Nullable
    @Column(name = "date_begin")
    private Date dateBeginTask;
    @Nullable
    @Column(name = "date_end")
    private Date dateEndTask;
}
