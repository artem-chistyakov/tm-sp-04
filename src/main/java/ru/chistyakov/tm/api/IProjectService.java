package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.dto.ProjectDTO;
import ru.chistyakov.tm.entity.Project;

import java.util.List;

public interface IProjectService {
    @Nullable
    Project merge(@Nullable Project project);

    @Nullable
    Project persist(@Nullable Project project);

    void removeAll();

    void remove(@Nullable String id);

    @Nullable Project findOne(@Nullable String id);


    @Nullable List<Project> findAll();

    @Nullable Project mapDtoToProject(@NotNull ProjectDTO projectDTO);

    @Nullable ProjectDTO mapProjectToDto(@NotNull Project project);
}
