package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.chistyakov.tm.dto.TaskDTO;
import ru.chistyakov.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {
    @Transactional
    @Nullable
    Task merge(@Nullable Task task);

    @Transactional
    @Nullable
    Task persist(@Nullable Task task);

    @Nullable Task findOne(@Nullable String id);

    @Nullable
    List<Task> findByProjectId(@Nullable String projectId);

    void remove(@Nullable Task task);

    void removeAll();

    @Nullable Collection<Task> findAll();

    @NotNull Task mapDtoToTask(@NotNull TaskDTO taskDTO);

    @NotNull TaskDTO mapTaskToDto(@NotNull Task task);
}
