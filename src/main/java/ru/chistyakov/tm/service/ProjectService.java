package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.chistyakov.tm.dto.ProjectDTO;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.repository.IProjectRepository;
import ru.chistyakov.tm.repository.ITaskRepository;

import java.util.List;

@Service
public class ProjectService implements ru.chistyakov.tm.api.IProjectService {

    private IProjectRepository projectRepository;
    private ITaskRepository taskRepository;

    @Autowired
    public void setProjectRepository(@NotNull final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Autowired
    public void setTaskRepository(@NotNull final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    @Transactional
    @Nullable
    public Project merge(@Nullable final Project project) {
        if (project == null) return null;
        return projectRepository.save(project);
    }

    @Override
    @Transactional
    @Nullable
    public Project persist(@Nullable final Project project) {
        if (project == null) return null;
        return projectRepository.save(project);
    }

    @Override
    @Transactional
    public void removeAll() {
        projectRepository.deleteAll();
    }

    @Override
    @Transactional
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        projectRepository.deleteById(id);
    }

    @Override
    public @Nullable Project findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findById(id).get();
    }

    @Override
    public @Nullable List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public @Nullable Project mapDtoToProject(final @NotNull ProjectDTO projectDTO) {
        final Project project = new Project();
        project.setId(projectDTO.getId());
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setDateBeginProject(projectDTO.getDateBeginProject());
        project.setDateEndProject(projectDTO.getDateEndProject());
        project.setTasks(taskRepository.findByProjectId(projectDTO.getId()));
        project.setReadinessStatus(projectDTO.getReadinessStatus());
        return project;
    }

    @Override
    public @Nullable ProjectDTO mapProjectToDto(final @NotNull Project project) {
        final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setDateBeginProject(project.getDateBeginProject());
        projectDTO.setDateEndProject(projectDTO.getDateEndProject());
        projectDTO.setReadinessStatus(project.getReadinessStatus());
        return projectDTO;
    }
}