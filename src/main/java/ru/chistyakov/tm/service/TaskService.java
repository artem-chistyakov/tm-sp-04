package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.chistyakov.tm.dto.TaskDTO;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.repository.IProjectRepository;
import ru.chistyakov.tm.repository.ITaskRepository;

import java.util.List;

@Service
public class TaskService implements ru.chistyakov.tm.api.ITaskService {

    private ITaskRepository taskRepository;
    private IProjectRepository projectRepository;

    @Autowired
    public void setTaskRepository(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Autowired
    public void setProjectRepository(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public @Nullable List<Task> findByProjectId(@Nullable String projectId) {
        if (projectId == null) return null;
        else return taskRepository.findByProjectId(projectId);
    }

    @Override
    @Transactional
    public Task merge(@Nullable final Task task) {
        if (task == null) return null;
        return taskRepository.save(task);
    }

    @Override
    @Transactional
    public Task persist(@Nullable final Task task) {
        if (task == null) return null;
        return taskRepository.save(task);
    }

    @Override
    public @Nullable Task findOne(@Nullable final String id) {
        if (id == null) return null;
        return taskRepository.findById(id).get();
    }


    @Override
    @Transactional
    public void remove(@Nullable final Task task) {
        taskRepository.delete(task);
    }

    @Override
    @Transactional
    public void removeAll() {
        taskRepository.deleteAll();
    }

    @Override
    public @Nullable List<Task> findAll() {
        return taskRepository.findAll();
    }

    public @NotNull Task mapDtoToTask(final @NotNull TaskDTO taskDTO) {
        final Task task = new Task();
        task.setId(taskDTO.getId());
        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        task.setDateBeginTask(taskDTO.getDateBeginTask());
        task.setDateEndTask(taskDTO.getDateEndTask());
        task.setProject(projectRepository.findById(taskDTO.getProjectId()).get());
        task.setReadinessStatus(taskDTO.getReadinessStatus());
        return task;
    }

    public @NotNull TaskDTO mapTaskToDto(final @NotNull Task task) {
        final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setDateBeginTask(task.getDateBeginTask());
        taskDTO.setDateEndTask(task.getDateEndTask());
        taskDTO.setProjectId(task.getProject().getId());
        taskDTO.setReadinessStatus(taskDTO.getReadinessStatus());
        return taskDTO;
    }
}
