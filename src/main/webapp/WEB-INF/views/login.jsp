<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content=" Пользовательская форма и дизайн для простой формы входа. Версия v4.0.0">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Страница входа </title>
    <link href="https://bootstrap-4.ru/docs/4.0/examples/sign-in/signin.css">
    <link rel="stylesheet"
          href="http://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <meta charset="UTF-8">
    <style>
        html, body {
            width: 100%;
            height: 100%;
            margin: 0
        }

        #action_form {
            position: absolute;
            width: 200px;
            height: 150px;
            left: 50%;
            top: 30%;
            margin-left: -100px;
            margin-top: -100px;
            border: 1px solid
        }

        form {
            padding: 14px
        }
    </style>
</head>
<body class="text-center">
<div id="action_form">
    <form:form class="form-signin" action="${pageContext.request.contextPath}/login" method="post">
        <img class="mb-4" src="<c:url value="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSDxy1WZtFLQo5nllYtcleDJAqnqtKYpfivHlq9kj-J_lIFvlkK"/>" alt=""
             width="110" height="110">
        <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
        <label for="username" class="sr-only">Username</label>
        <input type="text" name="username" id="username"class="form-control" placeholder="username" required
               autofocus>
        <label for="password" class="sr-only">Password</label>
        <input id="password" type="password" name="password" class="form-control" placeholder="Password" required/>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
    </form:form>
    <a href="${pageContext.request.contextPath}/registration">add new user</a>
</div>
</body>
</html>